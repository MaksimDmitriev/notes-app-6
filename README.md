# README #

iOS deployment target: 13.0

## version 2.1

*  XCode Version 13.2.1 (13C100)
*  Back arrow icon taken from: https://iconscout.com/icon/back-arrow-1767515, Licence: https://creativecommons.org/licenses/by/4.0/legalcode
*  Screen Navigation https://medium.com/bumble-tech/screen-navigation-in-ios-dd99b09228b2

### iPhone 13 Pro Max
https://youtu.be/Zekdli4iouE

### iPad (9th gen)
https://youtu.be/o3dduXsEjjg

## version 1.0

*  XCode Version 12.4 (12D4e)
*  MVI: https://broken-bytes.medium.com/using-the-mvi-pattern-in-swift-ios-app-development-72d7881d0dc2

![picture](screenshots/usage.gif)
