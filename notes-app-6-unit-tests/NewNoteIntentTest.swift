//
//  NewNoteIntentTest.swift
//  notes-app-6-unit-tests
//
//  Created by Maksim Dmitriev on 25/6/22.
//

import XCTest
@testable import notes_app_6
import Cuckoo

import RxSwift
import RxCocoa
import RxTest
import RxBlocking

class NewNoteIntentTest : XCTestCase {
    
    func test_bind() {
        let interactor = MockNotesInteractor()
        let screenRouter = MockScreenRouter()
        
        let stateRelay = PublishRelay<NoteState>()
        let disposeBag = DisposeBag()
        
        let intent = NewNoteIntent(
            notesInteractor: interactor,
            screenRouter: screenRouter,
            stateRelay: stateRelay,
            disposeBag: disposeBag
        )
        
        // configure handler
        let handler = MockNoteStateHandler()
        // TODO: ask Amri how they handle such cases.
        // I couldn't pass noteState because I needed to NoteState and NoteAdded to be equatable.
        stub(handler) { stub in
            when(stub.handleState(state: any()))
                .thenDoNothing()
        }
        
        let scheduler = TestScheduler(initialClock: 0)
        let result = scheduler.createObserver(NoteState.self)
        
        let noteState = NoteAdded(insertions: [1])
        
        intent.stateRelay
            .asDriver(onErrorJustReturn: NoteStateDefaultWrapper.DEFAULT)
            .drive(result)
            .disposed(by: disposeBag)
        
        intent.bind(toView: handler)
        intent.stateRelay.accept(noteState)
        
        scheduler.start()

        // XCTAssertEqual(result.events, [.next(0, noteState)])
    }
}
