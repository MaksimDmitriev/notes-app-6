//
//  NotesInteractorImplTest.swift
//  notes-app-6-unit-tests
//
//  Created by Maksim Dmitriev on 4/6/22.
//
import XCTest

@testable import notes_app_6
import Cuckoo

class NotesInteractorImplTest : XCTestCase {
    
    private let mockRepository = MockNotesRepository()
    
    func test_add_too_long_text() throws {
        let impl = NotesInteractorImpl(
            repository: mockRepository
        )
        
        let maxLengthTitle = "000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"
        XCTAssertEqual(Note.NOTE_TITLE_MAX_LENGTH, maxLengthTitle.count)
        let text = maxLengthTitle + "1"
        
        let maxLengthSummary = "000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"
        XCTAssertEqual(maxLengthSummary.count, Note.NOTE_SUMMARY_MAX_LENGTH)
        
        stub(mockRepository) { stub in
            when(stub.add(noteText: maxLengthTitle, summary: maxLengthSummary))
                .thenDoNothing()
        }
        
        impl.add(noteText: text, noteSummary: maxLengthSummary)
        
        verify(mockRepository)
            .add(noteText: maxLengthTitle, summary: maxLengthSummary)
    }
    
    func test_add_too_long_summary() throws {
        let impl = NotesInteractorImpl(
            repository: mockRepository
        )
        
        let maxLengthTitle = "000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"
        XCTAssertEqual(Note.NOTE_TITLE_MAX_LENGTH, maxLengthTitle.count)

        let maxLengthSummary = "000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"
        XCTAssertEqual(maxLengthSummary.count, Note.NOTE_SUMMARY_MAX_LENGTH)
        let summary = maxLengthSummary + "1"
        
        stub(mockRepository) { stub in
            when(stub.add(noteText: maxLengthTitle, summary: maxLengthSummary))
                .thenDoNothing()
        }
        
        impl.add(noteText: maxLengthTitle, noteSummary: summary)
        
        verify(mockRepository)
            .add(noteText: maxLengthTitle, summary: maxLengthSummary)
    }
    
    func test_add_null_text() throws {
        let impl = NotesInteractorImpl(
            repository: mockRepository
        )

        let summary = "sumamry"
        
        stub(mockRepository) { stub in
            when(stub.add(noteText: "", summary: summary))
                .thenDoNothing()
        }
        
        impl.add(noteText: nil, noteSummary: summary)
        
        verify(mockRepository)
            .add(noteText: "", summary: summary)
    }
    
    func test_add_happy_path() throws {
        let impl = NotesInteractorImpl(
            repository: mockRepository
        )
        
        let title = "title"
        let summary = "sumamry"
        
        stub(mockRepository) { stub in
            when(stub.add(noteText: title, summary: summary))
                .thenDoNothing()
        }
        
        impl.add(noteText: title, noteSummary: summary)
        
        verify(mockRepository)
            .add(noteText: title, summary: summary)
    }
}
