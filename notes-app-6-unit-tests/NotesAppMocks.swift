// MARK: - Mocks generated from file: notes-app-6/NoteStateHandler.swift at 2022-07-11 11:20:30 +0000

//
//  ViewControllerStateHandler.swift
//  notes-app-6
//
//  Created by Maksim Dmitriev on 28/5/22.
//

import Cuckoo
@testable import notes_app_6






 class MockNoteStateHandler: NoteStateHandler, Cuckoo.ProtocolMock {
    
     typealias MocksType = NoteStateHandler
    
     typealias Stubbing = __StubbingProxy_NoteStateHandler
     typealias Verification = __VerificationProxy_NoteStateHandler

     let cuckoo_manager = Cuckoo.MockManager.preconfiguredManager ?? Cuckoo.MockManager(hasParent: false)

    
    private var __defaultImplStub: NoteStateHandler?

     func enableDefaultImplementation(_ stub: NoteStateHandler) {
        __defaultImplStub = stub
        cuckoo_manager.enableDefaultStubImplementation()
    }
    

    

    

    
    
    
    
     func handleState(state: NoteState)  {
        
    return cuckoo_manager.call(
    """
    handleState(state: NoteState)
    """,
            parameters: (state),
            escapingParameters: (state),
            superclassCall:
                
                Cuckoo.MockManager.crashOnProtocolSuperclassCall()
                ,
            defaultCall: __defaultImplStub!.handleState(state: state))
        
    }
    
    

     struct __StubbingProxy_NoteStateHandler: Cuckoo.StubbingProxy {
        private let cuckoo_manager: Cuckoo.MockManager
    
         init(manager: Cuckoo.MockManager) {
            self.cuckoo_manager = manager
        }
        
        
        
        
        func handleState<M1: Cuckoo.Matchable>(state: M1) -> Cuckoo.ProtocolStubNoReturnFunction<(NoteState)> where M1.MatchedType == NoteState {
            let matchers: [Cuckoo.ParameterMatcher<(NoteState)>] = [wrap(matchable: state) { $0 }]
            return .init(stub: cuckoo_manager.createStub(for: MockNoteStateHandler.self, method:
    """
    handleState(state: NoteState)
    """, parameterMatchers: matchers))
        }
        
        
    }

     struct __VerificationProxy_NoteStateHandler: Cuckoo.VerificationProxy {
        private let cuckoo_manager: Cuckoo.MockManager
        private let callMatcher: Cuckoo.CallMatcher
        private let sourceLocation: Cuckoo.SourceLocation
    
         init(manager: Cuckoo.MockManager, callMatcher: Cuckoo.CallMatcher, sourceLocation: Cuckoo.SourceLocation) {
            self.cuckoo_manager = manager
            self.callMatcher = callMatcher
            self.sourceLocation = sourceLocation
        }
    
        
    
        
        
        
        @discardableResult
        func handleState<M1: Cuckoo.Matchable>(state: M1) -> Cuckoo.__DoNotUse<(NoteState), Void> where M1.MatchedType == NoteState {
            let matchers: [Cuckoo.ParameterMatcher<(NoteState)>] = [wrap(matchable: state) { $0 }]
            return cuckoo_manager.verify(
    """
    handleState(state: NoteState)
    """, callMatcher: callMatcher, parameterMatchers: matchers, sourceLocation: sourceLocation)
        }
        
        
    }
}

 class NoteStateHandlerStub: NoteStateHandler {
    

    

    
    
    
    
     func handleState(state: NoteState)   {
        return DefaultValueRegistry.defaultValue(for: (Void).self)
    }
    
    
}





// MARK: - Mocks generated from file: notes-app-6/NotesInteractor.swift at 2022-07-11 11:20:30 +0000

//
//  NotesInteractor.swift
//  notes-app-6
//
//  Created by Maksim Dmitriev on 4/6/22.
//

import Cuckoo
@testable import notes_app_6






 class MockNotesInteractor: NotesInteractor, Cuckoo.ProtocolMock {
    
     typealias MocksType = NotesInteractor
    
     typealias Stubbing = __StubbingProxy_NotesInteractor
     typealias Verification = __VerificationProxy_NotesInteractor

     let cuckoo_manager = Cuckoo.MockManager.preconfiguredManager ?? Cuckoo.MockManager(hasParent: false)

    
    private var __defaultImplStub: NotesInteractor?

     func enableDefaultImplementation(_ stub: NotesInteractor) {
        __defaultImplStub = stub
        cuckoo_manager.enableDefaultStubImplementation()
    }
    

    

    

    
    
    
    
     func add(noteText: String?, noteSummary: String)  {
        
    return cuckoo_manager.call(
    """
    add(noteText: String?, noteSummary: String)
    """,
            parameters: (noteText, noteSummary),
            escapingParameters: (noteText, noteSummary),
            superclassCall:
                
                Cuckoo.MockManager.crashOnProtocolSuperclassCall()
                ,
            defaultCall: __defaultImplStub!.add(noteText: noteText, noteSummary: noteSummary))
        
    }
    
    
    
    
    
     func remove(note: Note)  {
        
    return cuckoo_manager.call(
    """
    remove(note: Note)
    """,
            parameters: (note),
            escapingParameters: (note),
            superclassCall:
                
                Cuckoo.MockManager.crashOnProtocolSuperclassCall()
                ,
            defaultCall: __defaultImplStub!.remove(note: note))
        
    }
    
    
    
    
    
     func observeChanges()  {
        
    return cuckoo_manager.call(
    """
    observeChanges()
    """,
            parameters: (),
            escapingParameters: (),
            superclassCall:
                
                Cuckoo.MockManager.crashOnProtocolSuperclassCall()
                ,
            defaultCall: __defaultImplStub!.observeChanges())
        
    }
    
    
    
    
    
     func invalidateNotificationToken()  {
        
    return cuckoo_manager.call(
    """
    invalidateNotificationToken()
    """,
            parameters: (),
            escapingParameters: (),
            superclassCall:
                
                Cuckoo.MockManager.crashOnProtocolSuperclassCall()
                ,
            defaultCall: __defaultImplStub!.invalidateNotificationToken())
        
    }
    
    
    
    
    
     func objectAtIndexPath(index: Int) -> Note {
        
    return cuckoo_manager.call(
    """
    objectAtIndexPath(index: Int) -> Note
    """,
            parameters: (index),
            escapingParameters: (index),
            superclassCall:
                
                Cuckoo.MockManager.crashOnProtocolSuperclassCall()
                ,
            defaultCall: __defaultImplStub!.objectAtIndexPath(index: index))
        
    }
    
    
    
    
    
     func getCount() -> Int {
        
    return cuckoo_manager.call(
    """
    getCount() -> Int
    """,
            parameters: (),
            escapingParameters: (),
            superclassCall:
                
                Cuckoo.MockManager.crashOnProtocolSuperclassCall()
                ,
            defaultCall: __defaultImplStub!.getCount())
        
    }
    
    

     struct __StubbingProxy_NotesInteractor: Cuckoo.StubbingProxy {
        private let cuckoo_manager: Cuckoo.MockManager
    
         init(manager: Cuckoo.MockManager) {
            self.cuckoo_manager = manager
        }
        
        
        
        
        func add<M1: Cuckoo.OptionalMatchable, M2: Cuckoo.Matchable>(noteText: M1, noteSummary: M2) -> Cuckoo.ProtocolStubNoReturnFunction<(String?, String)> where M1.OptionalMatchedType == String, M2.MatchedType == String {
            let matchers: [Cuckoo.ParameterMatcher<(String?, String)>] = [wrap(matchable: noteText) { $0.0 }, wrap(matchable: noteSummary) { $0.1 }]
            return .init(stub: cuckoo_manager.createStub(for: MockNotesInteractor.self, method:
    """
    add(noteText: String?, noteSummary: String)
    """, parameterMatchers: matchers))
        }
        
        
        
        
        func remove<M1: Cuckoo.Matchable>(note: M1) -> Cuckoo.ProtocolStubNoReturnFunction<(Note)> where M1.MatchedType == Note {
            let matchers: [Cuckoo.ParameterMatcher<(Note)>] = [wrap(matchable: note) { $0 }]
            return .init(stub: cuckoo_manager.createStub(for: MockNotesInteractor.self, method:
    """
    remove(note: Note)
    """, parameterMatchers: matchers))
        }
        
        
        
        
        func observeChanges() -> Cuckoo.ProtocolStubNoReturnFunction<()> {
            let matchers: [Cuckoo.ParameterMatcher<Void>] = []
            return .init(stub: cuckoo_manager.createStub(for: MockNotesInteractor.self, method:
    """
    observeChanges()
    """, parameterMatchers: matchers))
        }
        
        
        
        
        func invalidateNotificationToken() -> Cuckoo.ProtocolStubNoReturnFunction<()> {
            let matchers: [Cuckoo.ParameterMatcher<Void>] = []
            return .init(stub: cuckoo_manager.createStub(for: MockNotesInteractor.self, method:
    """
    invalidateNotificationToken()
    """, parameterMatchers: matchers))
        }
        
        
        
        
        func objectAtIndexPath<M1: Cuckoo.Matchable>(index: M1) -> Cuckoo.ProtocolStubFunction<(Int), Note> where M1.MatchedType == Int {
            let matchers: [Cuckoo.ParameterMatcher<(Int)>] = [wrap(matchable: index) { $0 }]
            return .init(stub: cuckoo_manager.createStub(for: MockNotesInteractor.self, method:
    """
    objectAtIndexPath(index: Int) -> Note
    """, parameterMatchers: matchers))
        }
        
        
        
        
        func getCount() -> Cuckoo.ProtocolStubFunction<(), Int> {
            let matchers: [Cuckoo.ParameterMatcher<Void>] = []
            return .init(stub: cuckoo_manager.createStub(for: MockNotesInteractor.self, method:
    """
    getCount() -> Int
    """, parameterMatchers: matchers))
        }
        
        
    }

     struct __VerificationProxy_NotesInteractor: Cuckoo.VerificationProxy {
        private let cuckoo_manager: Cuckoo.MockManager
        private let callMatcher: Cuckoo.CallMatcher
        private let sourceLocation: Cuckoo.SourceLocation
    
         init(manager: Cuckoo.MockManager, callMatcher: Cuckoo.CallMatcher, sourceLocation: Cuckoo.SourceLocation) {
            self.cuckoo_manager = manager
            self.callMatcher = callMatcher
            self.sourceLocation = sourceLocation
        }
    
        
    
        
        
        
        @discardableResult
        func add<M1: Cuckoo.OptionalMatchable, M2: Cuckoo.Matchable>(noteText: M1, noteSummary: M2) -> Cuckoo.__DoNotUse<(String?, String), Void> where M1.OptionalMatchedType == String, M2.MatchedType == String {
            let matchers: [Cuckoo.ParameterMatcher<(String?, String)>] = [wrap(matchable: noteText) { $0.0 }, wrap(matchable: noteSummary) { $0.1 }]
            return cuckoo_manager.verify(
    """
    add(noteText: String?, noteSummary: String)
    """, callMatcher: callMatcher, parameterMatchers: matchers, sourceLocation: sourceLocation)
        }
        
        
        
        
        @discardableResult
        func remove<M1: Cuckoo.Matchable>(note: M1) -> Cuckoo.__DoNotUse<(Note), Void> where M1.MatchedType == Note {
            let matchers: [Cuckoo.ParameterMatcher<(Note)>] = [wrap(matchable: note) { $0 }]
            return cuckoo_manager.verify(
    """
    remove(note: Note)
    """, callMatcher: callMatcher, parameterMatchers: matchers, sourceLocation: sourceLocation)
        }
        
        
        
        
        @discardableResult
        func observeChanges() -> Cuckoo.__DoNotUse<(), Void> {
            let matchers: [Cuckoo.ParameterMatcher<Void>] = []
            return cuckoo_manager.verify(
    """
    observeChanges()
    """, callMatcher: callMatcher, parameterMatchers: matchers, sourceLocation: sourceLocation)
        }
        
        
        
        
        @discardableResult
        func invalidateNotificationToken() -> Cuckoo.__DoNotUse<(), Void> {
            let matchers: [Cuckoo.ParameterMatcher<Void>] = []
            return cuckoo_manager.verify(
    """
    invalidateNotificationToken()
    """, callMatcher: callMatcher, parameterMatchers: matchers, sourceLocation: sourceLocation)
        }
        
        
        
        
        @discardableResult
        func objectAtIndexPath<M1: Cuckoo.Matchable>(index: M1) -> Cuckoo.__DoNotUse<(Int), Note> where M1.MatchedType == Int {
            let matchers: [Cuckoo.ParameterMatcher<(Int)>] = [wrap(matchable: index) { $0 }]
            return cuckoo_manager.verify(
    """
    objectAtIndexPath(index: Int) -> Note
    """, callMatcher: callMatcher, parameterMatchers: matchers, sourceLocation: sourceLocation)
        }
        
        
        
        
        @discardableResult
        func getCount() -> Cuckoo.__DoNotUse<(), Int> {
            let matchers: [Cuckoo.ParameterMatcher<Void>] = []
            return cuckoo_manager.verify(
    """
    getCount() -> Int
    """, callMatcher: callMatcher, parameterMatchers: matchers, sourceLocation: sourceLocation)
        }
        
        
    }
}

 class NotesInteractorStub: NotesInteractor {
    

    

    
    
    
    
     func add(noteText: String?, noteSummary: String)   {
        return DefaultValueRegistry.defaultValue(for: (Void).self)
    }
    
    
    
    
    
     func remove(note: Note)   {
        return DefaultValueRegistry.defaultValue(for: (Void).self)
    }
    
    
    
    
    
     func observeChanges()   {
        return DefaultValueRegistry.defaultValue(for: (Void).self)
    }
    
    
    
    
    
     func invalidateNotificationToken()   {
        return DefaultValueRegistry.defaultValue(for: (Void).self)
    }
    
    
    
    
    
     func objectAtIndexPath(index: Int) -> Note  {
        return DefaultValueRegistry.defaultValue(for: (Note).self)
    }
    
    
    
    
    
     func getCount() -> Int  {
        return DefaultValueRegistry.defaultValue(for: (Int).self)
    }
    
    
}





// MARK: - Mocks generated from file: notes-app-6/NotesRepository.swift at 2022-07-11 11:20:30 +0000

//
//  NotesRepository.swift
//  notes-app-6
//
//  Created by Maksim Dmitriev on 4/6/22.
//

import Cuckoo
@testable import notes_app_6






 class MockNotesRepository: NotesRepository, Cuckoo.ProtocolMock {
    
     typealias MocksType = NotesRepository
    
     typealias Stubbing = __StubbingProxy_NotesRepository
     typealias Verification = __VerificationProxy_NotesRepository

     let cuckoo_manager = Cuckoo.MockManager.preconfiguredManager ?? Cuckoo.MockManager(hasParent: false)

    
    private var __defaultImplStub: NotesRepository?

     func enableDefaultImplementation(_ stub: NotesRepository) {
        __defaultImplStub = stub
        cuckoo_manager.enableDefaultStubImplementation()
    }
    

    

    

    
    
    
    
     func add(noteText: String, summary: String)  {
        
    return cuckoo_manager.call(
    """
    add(noteText: String, summary: String)
    """,
            parameters: (noteText, summary),
            escapingParameters: (noteText, summary),
            superclassCall:
                
                Cuckoo.MockManager.crashOnProtocolSuperclassCall()
                ,
            defaultCall: __defaultImplStub!.add(noteText: noteText, summary: summary))
        
    }
    
    
    
    
    
     func remove(note: Note)  {
        
    return cuckoo_manager.call(
    """
    remove(note: Note)
    """,
            parameters: (note),
            escapingParameters: (note),
            superclassCall:
                
                Cuckoo.MockManager.crashOnProtocolSuperclassCall()
                ,
            defaultCall: __defaultImplStub!.remove(note: note))
        
    }
    
    
    
    
    
     func objectAtIndexPath(index: Int) -> Note {
        
    return cuckoo_manager.call(
    """
    objectAtIndexPath(index: Int) -> Note
    """,
            parameters: (index),
            escapingParameters: (index),
            superclassCall:
                
                Cuckoo.MockManager.crashOnProtocolSuperclassCall()
                ,
            defaultCall: __defaultImplStub!.objectAtIndexPath(index: index))
        
    }
    
    
    
    
    
     func getCount() -> Int {
        
    return cuckoo_manager.call(
    """
    getCount() -> Int
    """,
            parameters: (),
            escapingParameters: (),
            superclassCall:
                
                Cuckoo.MockManager.crashOnProtocolSuperclassCall()
                ,
            defaultCall: __defaultImplStub!.getCount())
        
    }
    
    
    
    
    
     func observeChanges()  {
        
    return cuckoo_manager.call(
    """
    observeChanges()
    """,
            parameters: (),
            escapingParameters: (),
            superclassCall:
                
                Cuckoo.MockManager.crashOnProtocolSuperclassCall()
                ,
            defaultCall: __defaultImplStub!.observeChanges())
        
    }
    
    
    
    
    
     func invalidateNotificationToken()  {
        
    return cuckoo_manager.call(
    """
    invalidateNotificationToken()
    """,
            parameters: (),
            escapingParameters: (),
            superclassCall:
                
                Cuckoo.MockManager.crashOnProtocolSuperclassCall()
                ,
            defaultCall: __defaultImplStub!.invalidateNotificationToken())
        
    }
    
    

     struct __StubbingProxy_NotesRepository: Cuckoo.StubbingProxy {
        private let cuckoo_manager: Cuckoo.MockManager
    
         init(manager: Cuckoo.MockManager) {
            self.cuckoo_manager = manager
        }
        
        
        
        
        func add<M1: Cuckoo.Matchable, M2: Cuckoo.Matchable>(noteText: M1, summary: M2) -> Cuckoo.ProtocolStubNoReturnFunction<(String, String)> where M1.MatchedType == String, M2.MatchedType == String {
            let matchers: [Cuckoo.ParameterMatcher<(String, String)>] = [wrap(matchable: noteText) { $0.0 }, wrap(matchable: summary) { $0.1 }]
            return .init(stub: cuckoo_manager.createStub(for: MockNotesRepository.self, method:
    """
    add(noteText: String, summary: String)
    """, parameterMatchers: matchers))
        }
        
        
        
        
        func remove<M1: Cuckoo.Matchable>(note: M1) -> Cuckoo.ProtocolStubNoReturnFunction<(Note)> where M1.MatchedType == Note {
            let matchers: [Cuckoo.ParameterMatcher<(Note)>] = [wrap(matchable: note) { $0 }]
            return .init(stub: cuckoo_manager.createStub(for: MockNotesRepository.self, method:
    """
    remove(note: Note)
    """, parameterMatchers: matchers))
        }
        
        
        
        
        func objectAtIndexPath<M1: Cuckoo.Matchable>(index: M1) -> Cuckoo.ProtocolStubFunction<(Int), Note> where M1.MatchedType == Int {
            let matchers: [Cuckoo.ParameterMatcher<(Int)>] = [wrap(matchable: index) { $0 }]
            return .init(stub: cuckoo_manager.createStub(for: MockNotesRepository.self, method:
    """
    objectAtIndexPath(index: Int) -> Note
    """, parameterMatchers: matchers))
        }
        
        
        
        
        func getCount() -> Cuckoo.ProtocolStubFunction<(), Int> {
            let matchers: [Cuckoo.ParameterMatcher<Void>] = []
            return .init(stub: cuckoo_manager.createStub(for: MockNotesRepository.self, method:
    """
    getCount() -> Int
    """, parameterMatchers: matchers))
        }
        
        
        
        
        func observeChanges() -> Cuckoo.ProtocolStubNoReturnFunction<()> {
            let matchers: [Cuckoo.ParameterMatcher<Void>] = []
            return .init(stub: cuckoo_manager.createStub(for: MockNotesRepository.self, method:
    """
    observeChanges()
    """, parameterMatchers: matchers))
        }
        
        
        
        
        func invalidateNotificationToken() -> Cuckoo.ProtocolStubNoReturnFunction<()> {
            let matchers: [Cuckoo.ParameterMatcher<Void>] = []
            return .init(stub: cuckoo_manager.createStub(for: MockNotesRepository.self, method:
    """
    invalidateNotificationToken()
    """, parameterMatchers: matchers))
        }
        
        
    }

     struct __VerificationProxy_NotesRepository: Cuckoo.VerificationProxy {
        private let cuckoo_manager: Cuckoo.MockManager
        private let callMatcher: Cuckoo.CallMatcher
        private let sourceLocation: Cuckoo.SourceLocation
    
         init(manager: Cuckoo.MockManager, callMatcher: Cuckoo.CallMatcher, sourceLocation: Cuckoo.SourceLocation) {
            self.cuckoo_manager = manager
            self.callMatcher = callMatcher
            self.sourceLocation = sourceLocation
        }
    
        
    
        
        
        
        @discardableResult
        func add<M1: Cuckoo.Matchable, M2: Cuckoo.Matchable>(noteText: M1, summary: M2) -> Cuckoo.__DoNotUse<(String, String), Void> where M1.MatchedType == String, M2.MatchedType == String {
            let matchers: [Cuckoo.ParameterMatcher<(String, String)>] = [wrap(matchable: noteText) { $0.0 }, wrap(matchable: summary) { $0.1 }]
            return cuckoo_manager.verify(
    """
    add(noteText: String, summary: String)
    """, callMatcher: callMatcher, parameterMatchers: matchers, sourceLocation: sourceLocation)
        }
        
        
        
        
        @discardableResult
        func remove<M1: Cuckoo.Matchable>(note: M1) -> Cuckoo.__DoNotUse<(Note), Void> where M1.MatchedType == Note {
            let matchers: [Cuckoo.ParameterMatcher<(Note)>] = [wrap(matchable: note) { $0 }]
            return cuckoo_manager.verify(
    """
    remove(note: Note)
    """, callMatcher: callMatcher, parameterMatchers: matchers, sourceLocation: sourceLocation)
        }
        
        
        
        
        @discardableResult
        func objectAtIndexPath<M1: Cuckoo.Matchable>(index: M1) -> Cuckoo.__DoNotUse<(Int), Note> where M1.MatchedType == Int {
            let matchers: [Cuckoo.ParameterMatcher<(Int)>] = [wrap(matchable: index) { $0 }]
            return cuckoo_manager.verify(
    """
    objectAtIndexPath(index: Int) -> Note
    """, callMatcher: callMatcher, parameterMatchers: matchers, sourceLocation: sourceLocation)
        }
        
        
        
        
        @discardableResult
        func getCount() -> Cuckoo.__DoNotUse<(), Int> {
            let matchers: [Cuckoo.ParameterMatcher<Void>] = []
            return cuckoo_manager.verify(
    """
    getCount() -> Int
    """, callMatcher: callMatcher, parameterMatchers: matchers, sourceLocation: sourceLocation)
        }
        
        
        
        
        @discardableResult
        func observeChanges() -> Cuckoo.__DoNotUse<(), Void> {
            let matchers: [Cuckoo.ParameterMatcher<Void>] = []
            return cuckoo_manager.verify(
    """
    observeChanges()
    """, callMatcher: callMatcher, parameterMatchers: matchers, sourceLocation: sourceLocation)
        }
        
        
        
        
        @discardableResult
        func invalidateNotificationToken() -> Cuckoo.__DoNotUse<(), Void> {
            let matchers: [Cuckoo.ParameterMatcher<Void>] = []
            return cuckoo_manager.verify(
    """
    invalidateNotificationToken()
    """, callMatcher: callMatcher, parameterMatchers: matchers, sourceLocation: sourceLocation)
        }
        
        
    }
}

 class NotesRepositoryStub: NotesRepository {
    

    

    
    
    
    
     func add(noteText: String, summary: String)   {
        return DefaultValueRegistry.defaultValue(for: (Void).self)
    }
    
    
    
    
    
     func remove(note: Note)   {
        return DefaultValueRegistry.defaultValue(for: (Void).self)
    }
    
    
    
    
    
     func objectAtIndexPath(index: Int) -> Note  {
        return DefaultValueRegistry.defaultValue(for: (Note).self)
    }
    
    
    
    
    
     func getCount() -> Int  {
        return DefaultValueRegistry.defaultValue(for: (Int).self)
    }
    
    
    
    
    
     func observeChanges()   {
        return DefaultValueRegistry.defaultValue(for: (Void).self)
    }
    
    
    
    
    
     func invalidateNotificationToken()   {
        return DefaultValueRegistry.defaultValue(for: (Void).self)
    }
    
    
}





// MARK: - Mocks generated from file: notes-app-6/ScreenRouter.swift at 2022-07-11 11:20:30 +0000

//
//  ScreenRouter.swift
//  notes-app-6
//
//  Created by Maksim Dmitriev on 3/5/22.
//

import Cuckoo
@testable import notes_app_6

import UIKit






 class MockScreenRouter: ScreenRouter, Cuckoo.ProtocolMock {
    
     typealias MocksType = ScreenRouter
    
     typealias Stubbing = __StubbingProxy_ScreenRouter
     typealias Verification = __VerificationProxy_ScreenRouter

     let cuckoo_manager = Cuckoo.MockManager.preconfiguredManager ?? Cuckoo.MockManager(hasParent: false)

    
    private var __defaultImplStub: ScreenRouter?

     func enableDefaultImplementation(_ stub: ScreenRouter) {
        __defaultImplStub = stub
        cuckoo_manager.enableDefaultStubImplementation()
    }
    

    

    

    
    
    
    
     func present(uiViewController: UIViewController, viewControllerToPresent: UIViewController, animated flag: Bool)  {
        
    return cuckoo_manager.call(
    """
    present(uiViewController: UIViewController, viewControllerToPresent: UIViewController, animated: Bool)
    """,
            parameters: (uiViewController, viewControllerToPresent, flag),
            escapingParameters: (uiViewController, viewControllerToPresent, flag),
            superclassCall:
                
                Cuckoo.MockManager.crashOnProtocolSuperclassCall()
                ,
            defaultCall: __defaultImplStub!.present(uiViewController: uiViewController, viewControllerToPresent: viewControllerToPresent, animated: flag))
        
    }
    
    
    
    
    
     func dismiss(dismissFunc: (() -> Void))  {
        return withoutActuallyEscaping(dismissFunc, do: { (dismissFunc: @escaping (() -> Void)) -> Void in

    return cuckoo_manager.call(
    """
    dismiss(dismissFunc: (() -> Void))
    """,
            parameters: (dismissFunc),
            escapingParameters: ({ _ in fatalError("This is a stub! It's not supposed to be called!") }),
            superclassCall:
                
                Cuckoo.MockManager.crashOnProtocolSuperclassCall()
                ,
            defaultCall: __defaultImplStub!.dismiss(dismissFunc: dismissFunc))
        })

    }
    
    

     struct __StubbingProxy_ScreenRouter: Cuckoo.StubbingProxy {
        private let cuckoo_manager: Cuckoo.MockManager
    
         init(manager: Cuckoo.MockManager) {
            self.cuckoo_manager = manager
        }
        
        
        
        
        func present<M1: Cuckoo.Matchable, M2: Cuckoo.Matchable, M3: Cuckoo.Matchable>(uiViewController: M1, viewControllerToPresent: M2, animated flag: M3) -> Cuckoo.ProtocolStubNoReturnFunction<(UIViewController, UIViewController, Bool)> where M1.MatchedType == UIViewController, M2.MatchedType == UIViewController, M3.MatchedType == Bool {
            let matchers: [Cuckoo.ParameterMatcher<(UIViewController, UIViewController, Bool)>] = [wrap(matchable: uiViewController) { $0.0 }, wrap(matchable: viewControllerToPresent) { $0.1 }, wrap(matchable: flag) { $0.2 }]
            return .init(stub: cuckoo_manager.createStub(for: MockScreenRouter.self, method:
    """
    present(uiViewController: UIViewController, viewControllerToPresent: UIViewController, animated: Bool)
    """, parameterMatchers: matchers))
        }
        
        
        
        
        func dismiss<M1: Cuckoo.Matchable>(dismissFunc: M1) -> Cuckoo.ProtocolStubNoReturnFunction<((() -> Void))> where M1.MatchedType == (() -> Void) {
            let matchers: [Cuckoo.ParameterMatcher<((() -> Void))>] = [wrap(matchable: dismissFunc) { $0 }]
            return .init(stub: cuckoo_manager.createStub(for: MockScreenRouter.self, method:
    """
    dismiss(dismissFunc: (() -> Void))
    """, parameterMatchers: matchers))
        }
        
        
    }

     struct __VerificationProxy_ScreenRouter: Cuckoo.VerificationProxy {
        private let cuckoo_manager: Cuckoo.MockManager
        private let callMatcher: Cuckoo.CallMatcher
        private let sourceLocation: Cuckoo.SourceLocation
    
         init(manager: Cuckoo.MockManager, callMatcher: Cuckoo.CallMatcher, sourceLocation: Cuckoo.SourceLocation) {
            self.cuckoo_manager = manager
            self.callMatcher = callMatcher
            self.sourceLocation = sourceLocation
        }
    
        
    
        
        
        
        @discardableResult
        func present<M1: Cuckoo.Matchable, M2: Cuckoo.Matchable, M3: Cuckoo.Matchable>(uiViewController: M1, viewControllerToPresent: M2, animated flag: M3) -> Cuckoo.__DoNotUse<(UIViewController, UIViewController, Bool), Void> where M1.MatchedType == UIViewController, M2.MatchedType == UIViewController, M3.MatchedType == Bool {
            let matchers: [Cuckoo.ParameterMatcher<(UIViewController, UIViewController, Bool)>] = [wrap(matchable: uiViewController) { $0.0 }, wrap(matchable: viewControllerToPresent) { $0.1 }, wrap(matchable: flag) { $0.2 }]
            return cuckoo_manager.verify(
    """
    present(uiViewController: UIViewController, viewControllerToPresent: UIViewController, animated: Bool)
    """, callMatcher: callMatcher, parameterMatchers: matchers, sourceLocation: sourceLocation)
        }
        
        
        
        
        @discardableResult
        func dismiss<M1: Cuckoo.Matchable>(dismissFunc: M1) -> Cuckoo.__DoNotUse<((() -> Void)), Void> where M1.MatchedType == (() -> Void) {
            let matchers: [Cuckoo.ParameterMatcher<((() -> Void))>] = [wrap(matchable: dismissFunc) { $0 }]
            return cuckoo_manager.verify(
    """
    dismiss(dismissFunc: (() -> Void))
    """, callMatcher: callMatcher, parameterMatchers: matchers, sourceLocation: sourceLocation)
        }
        
        
    }
}

 class ScreenRouterStub: ScreenRouter {
    

    

    
    
    
    
     func present(uiViewController: UIViewController, viewControllerToPresent: UIViewController, animated flag: Bool)   {
        return DefaultValueRegistry.defaultValue(for: (Void).self)
    }
    
    
    
    
    
     func dismiss(dismissFunc: (() -> Void))   {
        return DefaultValueRegistry.defaultValue(for: (Void).self)
    }
    
    
}




