//
//  NoteInteractor.swift
//  notes-app-5
//
//  Created by Maksim Dmitriev on 17/4/21.
//  Copyright © 2021 Maksim Dmitriev. All rights reserved.
//

import RealmSwift
import RxSwift
import RxCocoa

class NotesInteractorImpl: NotesInteractor {
    
    private let repository : NotesRepository
    
    init(repository : NotesRepository) {
        self.repository = repository
    }
    
    func add(noteText: String?, noteSummary: String) {
        let text = validateString(input: noteText, maxLen: Note.NOTE_TITLE_MAX_LENGTH)
        let summary = validateString(input: noteSummary, maxLen: Note.NOTE_SUMMARY_MAX_LENGTH)
        repository.add(noteText: text, summary: summary)
    }
    
    private func validateString(input: String?, maxLen: Int) -> String {
        if let s = input {
            return s.count > maxLen ? String(s.prefix(maxLen)) : s
        }
        return ""
    }
    
    func remove(note: Note) {
        repository.remove(note: note)
    }
    
    func objectAtIndexPath(index: Int) -> Note {
        return repository.objectAtIndexPath(index: index)
    }
    
    func getCount() -> Int {
        return repository.getCount()
    }
    
    func observeChanges() {
        repository.observeChanges()
    }
    
    func invalidateNotificationToken() {
        repository.invalidateNotificationToken()
    }
}
