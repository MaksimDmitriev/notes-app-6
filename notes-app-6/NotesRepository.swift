//
//  NotesRepository.swift
//  notes-app-6
//
//  Created by Maksim Dmitriev on 4/6/22.
//

protocol NotesRepository {
    
    func add(noteText : String, summary: String)
    
    func remove(note: Note)
    
    func objectAtIndexPath(index: Int) -> Note
    
    func getCount() -> Int
    
    func observeChanges()
    
    func invalidateNotificationToken()
}
