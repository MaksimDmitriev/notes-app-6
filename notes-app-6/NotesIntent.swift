//
//  NotesIntent.swift
//  notes-app-5
//
//  Created by Maksim Dmitriev on 17/4/21.
//  Copyright © 2021 Maksim Dmitriev. All rights reserved.
//

import RxSwift
import RxCocoa
import RealmSwift

class NotesIntent : BaseIntent {
    
    public func onRemoveNote(note: Note) {
        notesInteractor.remove(note: note)
    }
    
    public func onNewNoteButtonClick(uiViewController: UIViewController) {
        let viewControllerToPresent = NewNoteUIViewController() { [weak uiViewController] in
            uiViewController?.dismiss(animated: true)
            let notesUIViewController = uiViewController as? NotesUIViewController
            if notesUIViewController != nil {
                notesUIViewController?.reloadData()
            }
        }
        // according to the doc,
        // https://developer.apple.com/documentation/uikit/uiviewcontroller/1621355-modalpresentationstyle
        // modalPresentationStyle is only used in regular-width size classes
        // TODO: check on all simualtors possible which counterpart devices are in current use.
        viewControllerToPresent.modalPresentationStyle = .fullScreen
        screenRouter.present(
            uiViewController: uiViewController,
            viewControllerToPresent: viewControllerToPresent,
            animated: true
        )
    }
    
    public func objectAtIndexPath(index: Int) -> Note {
        return notesInteractor.objectAtIndexPath(index: index)
    }
    
    public func getCount() -> Int {
        return notesInteractor.getCount()
    }
}
