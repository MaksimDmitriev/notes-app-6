//
//  UiUtils.swift
//  notes-app-6
//
//  Created by Maksim Dmitriev on 20/2/22.
//

import UIKit

extension UITraitCollection {
    
    func getDeviceClass() -> DeviceClass {
        let widthClass = self.horizontalSizeClass.rawValue
        let heightClass = self.verticalSizeClass.rawValue
                        
        if (widthClass == UIUserInterfaceSizeClass.regular.rawValue && heightClass == UIUserInterfaceSizeClass.regular.rawValue) {
            return DeviceClass.iPad
        }
        
        if (widthClass == UIUserInterfaceSizeClass.compact.rawValue && heightClass == UIUserInterfaceSizeClass.regular.rawValue) {
            return DeviceClass.iPhone
        }
        
        return DeviceClass.iPhoneLand
    }
}

enum DeviceClass {
    case iPad
    case iPhone
    case iPhoneLand
}

