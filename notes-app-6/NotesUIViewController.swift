//
//  APDynamicHeaderTableViewController.swift
//  notes-app-6
//
//  Created by Maksim Dmitriev on 26/4/21.
//

import UIKit
import SnapKit
import RxSwift
import RxCocoa

class NotesUIViewController : UIViewController, NoteStateHandler {
    
    private static let ALL_NOTES_VIEW_RATIO = 1 / 3.0
    private static let SUMMARY_VIEW_RATIO = 2 / 3.0
    
    private static let ITEMS_PER_ROW: CGFloat = 1
    
    private let noteTiltePlaceholder = NSLocalizedString(
        "empty_note_title_placeholder",
        comment: ""
    )
    private let noteSummaryPlaceholder = NSLocalizedString(
        "empty_note_summary_placeholder",
        comment: ""
    )
    
    // Properties
    private let stateRelay = PublishRelay<NoteState>()
    private let disposeBag = DisposeBag()
    private let intent: NotesIntent
    
    // Views
    private lazy var notesLabel : UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: label.font.pointSize)
        label.text = NSLocalizedString("notes", comment: "")
        return label
    }()
    
    private lazy var newNoteIcon : UIImageView = {
        let newNoteView = UIImageView()
        newNoteView.isUserInteractionEnabled = true
        newNoteView.image = UIImage(named: "new_note")
        let tapRecognizer = UITapGestureRecognizer(
            target: self,
            action: #selector(handleNewNoteIconTap)
        )
        newNoteView.addGestureRecognizer(tapRecognizer)
        return newNoteView
    }()
    
    private lazy var summaryView : UILabel = {
        let label = UILabel()
        return label
    }()
    
    // UICollectionView
    private lazy var allNotesUICollectionView = UITableView()
    private let cellLayout = UICollectionViewFlowLayout()
    
    init() {
        self.intent = NotesIntent(
            notesInteractor: NotesInteractorImpl(repository: NotesRepositoryImpl(stateRelay: stateRelay)),
            screenRouter: ScreenRouterImpl(),
            stateRelay: stateRelay,
            disposeBag: disposeBag
        )
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        self.intent = NotesIntent(
            notesInteractor: NotesInteractorImpl(repository: NotesRepositoryImpl(stateRelay: stateRelay)),
            screenRouter: ScreenRouterImpl(),
            stateRelay: stateRelay,
            disposeBag: disposeBag
        )
        super.init(coder: coder)
    }
    
    // Called once, when the screen is launched.
    // Not on each rotation
    override func loadView() {
        super.loadView()
        addNewNoteIcon()
        addNotesLabel()
        
        let deviceClass = self.traitCollection.getDeviceClass()
        addCollectionView(deviceClass: deviceClass)
        addSummaryView(deviceClass: deviceClass)
    }
    
    override func viewDidLoad() {
        intent.bind(toView: self)
    }
    
    @objc func handleNewNoteIconTap() {
        intent.onNewNoteButtonClick(uiViewController: self)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        intent.invalidateNotificationToken()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        intent.observeChanges()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        let deviceClass = self.traitCollection.getDeviceClass()
        setCollectionViewConstraints(deviceClass: deviceClass)
        setSummaryViewConstraints(deviceClass: deviceClass)
    }
    
    private func addNewNoteIcon() {
        view.addSubview(newNoteIcon)
        newNoteIcon.snp.makeConstraints { (make) in
            make.trailing.equalTo(view.safeAreaLayoutGuide.snp.trailing).offset(-UIConstants.PADDING_SMALL)
            make.top.equalTo(view.safeAreaLayoutGuide.snp.top)
        }
    }
    
    private func addNotesLabel() {
        view.addSubview(notesLabel)
        notesLabel.snp.makeConstraints { (make) in
            make.leading.equalTo(view.safeAreaLayoutGuide.snp.leading).offset(UIConstants.PADDING_SMALL)
            make.centerY.equalTo(newNoteIcon.snp.centerY)
        }
    }
    
    private func configureCell() {
        // Cell Layout Sizes
        cellLayout.scrollDirection = .vertical
        cellLayout.itemSize = getItemSize()
    }
    
    private func getItemSize() -> CGSize {
        let availableWidth = UIScreen.main.bounds.width
        let widthPerItem = availableWidth / NotesUIViewController.ITEMS_PER_ROW
        return CGSize(width: widthPerItem, height: widthPerItem / 2)
    }
    
    private func addCollectionView(deviceClass: DeviceClass) {
        configureCell()
        
        allNotesUICollectionView.frame = .zero
        
        view.addSubview(allNotesUICollectionView)
        
        setCollectionViewConstraints(deviceClass: deviceClass)
        
        configureCollectionView()
        allNotesUICollectionView.register(
            NoteItemViewCell.self,
            forCellReuseIdentifier: NoteItemViewCell.ID
        )
    }
    
    private func setCollectionViewConstraints(deviceClass: DeviceClass) {
        allNotesUICollectionView.snp.updateConstraints { (make) -> Void in
            make.top.equalTo(notesLabel.snp.bottom).offset(UIConstants.PADDING_SMALL)
            make.leading.equalTo(view.safeAreaLayoutGuide.snp.leading)
            make.width.equalTo(getAllNotesViewWidth(deviceClass: deviceClass))
            make.bottom.equalTo(view.safeAreaLayoutGuide.snp.bottom)
        }
    }
    
    private func getAllNotesViewWidth(deviceClass: DeviceClass) -> CGFloat {
        let screenWidth = UIScreen.main.bounds.width
        switch deviceClass {
        case .iPad, .iPhoneLand:
            return CGFloat(NotesUIViewController.ALL_NOTES_VIEW_RATIO) * screenWidth
        default:
            return screenWidth
        }
    }
    
    private func addSummaryView(deviceClass: DeviceClass) {
        view.addSubview(summaryView)
        setSummaryViewConstraints(deviceClass: deviceClass)
    }
    
    private func setSummaryViewConstraints(deviceClass: DeviceClass) {
        summaryView.snp.updateConstraints { (make) -> Void in
            make.top.equalTo(notesLabel.snp.bottom).offset(UIConstants.PADDING_SMALL)
            make.leading.equalTo(allNotesUICollectionView.snp.trailing)
            make.width.equalTo(getSummaryViewWidth(deviceClass: deviceClass))
        }
    }
    
    private func getSummaryViewWidth(deviceClass: DeviceClass) -> CGFloat {
        let screenWidth = UIScreen.main.bounds.width
        switch deviceClass {
        case .iPad, .iPhoneLand:
            return CGFloat(NotesUIViewController.SUMMARY_VIEW_RATIO) * screenWidth
        default:
            return 0
        }
    }
    
    public func reloadData() {
        allNotesUICollectionView.reloadData()
    }
    
    private func configureCollectionView() {
        allNotesUICollectionView.backgroundColor = .white
        allNotesUICollectionView.showsVerticalScrollIndicator = true
        allNotesUICollectionView.isScrollEnabled = true
        allNotesUICollectionView.bounces = true
        allNotesUICollectionView.delegate = self
        allNotesUICollectionView.dataSource = self
    }
    
    // handle the incoming MVI state
    func handleState(state: NoteState) {
        switch state {
        case is NoteRemoved:
            let noteRemoved = state as? NoteRemoved
            if (noteRemoved != nil) {
                // Query results have changed, so apply them to the UITableView
                let deletions = noteRemoved?.deletions ?? []
                allNotesUICollectionView.performBatchUpdates({
                    allNotesUICollectionView.deleteRows(
                        at: deletions.map({ IndexPath(row: $0, section: 0)}),
                        with: .fade
                    )
                }, completion: { finished in })
                summaryView.text = ""
            }
            break
        default:
            break
        }
    }
}

extension NotesUIViewController : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let note = intent.objectAtIndexPath(index: indexPath.row)
            intent.onRemoveNote(note: note)
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let note = intent.objectAtIndexPath(index: indexPath.row)
        let summary = note.summary
        summaryView.text = summary.isEmpty ? noteSummaryPlaceholder : summary
    }
}

extension NotesUIViewController : UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return intent.getCount()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: NoteItemViewCell.ID, for: indexPath) as! NoteItemViewCell
        let note = intent.objectAtIndexPath(index: indexPath.row)
        let text = note.text
        if (text.isEmpty) {
            cell.noteUILabel.text = noteTiltePlaceholder
        } else {
            cell.noteUILabel.text = text
        }
        return cell
    }
}
