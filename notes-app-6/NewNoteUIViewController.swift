//
//  NewNoteUIViewController.swift
//  notes-app-6
//
//  Created by Maksim Dmitriev on 30/1/22.
//

import UIKit
import RxSwift
import RxCocoa

class NewNoteUIViewController : UIViewController, NoteStateHandler {
    
    private static let NEW_NOTE_SUMMARY_FIELD_RADIUS = CGFloat(UIScreen.main.scale * 2.0)
    private static let NOTE_SUMMARY_HIGHT = UIScreen.main.scale * 96
    
    private let stateRelay = PublishRelay<NoteState>()
    private let disposeBag = DisposeBag()
    
    private let intent: NewNoteIntent
    
    private lazy var newNoteTitleTextField : UITextField = {
        let textField = UITextField()
        textField.borderStyle = .roundedRect
        
        let placeholderFormat = NSLocalizedString(
            "title_placeholder",
            comment: "Note title. Max %lu\(Note.NOTE_TITLE_MAX_LENGTH) symbols"
        )
        let placeholder = String.localizedStringWithFormat(
            placeholderFormat,
            Note.NOTE_TITLE_MAX_LENGTH
        )
        textField.placeholder = placeholder
        
        return textField
    }()
    
    private lazy var newNoteSummaryTextField : UITextView = {
        let textView = UITextView()
        textView.layer.cornerRadius = NewNoteUIViewController.NEW_NOTE_SUMMARY_FIELD_RADIUS
        return textView
    }()
    
    private lazy var saveButton: UIButton = {
        let button = UIButton(type: .system)
        let title = NSLocalizedString("save_button", comment: "")
        button.setTitle(title, for: .normal)
        return button
    }()
    
    private lazy var discardButton: UIButton = {
        let button = UIButton(type: .system)
        let title = NSLocalizedString("discard_button", comment: "")
        button.setTitle(title, for: .normal)
        return button
    }()
    
    private lazy var backArrowIcon : UIImageView = {
        let newNoteView = UIImageView()
        newNoteView.isUserInteractionEnabled = true
        newNoteView.image = UIImage(named: "back_arrow")
        let tapRecognizer = UITapGestureRecognizer(
            target: self,
            action: #selector(handleBackClick)
        )
        newNoteView.addGestureRecognizer(tapRecognizer)
        return newNoteView
    }()
    
    private let didFinish: () -> Void
    
    init(didFinish: @escaping () -> Void) {
        self.didFinish = didFinish
        self.intent = NewNoteIntent(
            notesInteractor: NotesInteractorImpl(repository: NotesRepositoryImpl(stateRelay: stateRelay)),
            screenRouter: ScreenRouterImpl(),
            stateRelay: stateRelay,
            disposeBag: disposeBag
        )
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        self.didFinish = { () in }
        self.intent = NewNoteIntent(
            notesInteractor: NotesInteractorImpl(repository: NotesRepositoryImpl(stateRelay: stateRelay)),
            screenRouter: ScreenRouterImpl(),
            stateRelay: stateRelay,
            disposeBag: disposeBag
        )
        super.init(coder: coder)
    }
    
    override func loadView() {
        super.loadView()
        
        addBackArrow()
        addTitleView()
        addSummaryView()
        addSaveButton()
        addDiscardButton()
        view.backgroundColor = UIColor(red: 0.9, green: 1, blue: 0.9, alpha: 1)
    }
    
    override func viewDidLoad() {
        intent.bind(toView: self)
        bindButtons()
        intent.observeChanges()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        intent.invalidateNotificationToken()
    }
    
    func handleState(state: NoteState) {
        switch state {
        case is NoteAdded, is NewNoteDiscarded:
            resetTextFields()
            break
        default:
            break
        }
    }
    
    private func resetTextFields() {
        self.newNoteTitleTextField.text = ""
        self.newNoteSummaryTextField.text = ""
    }
    
    private func addTitleView() {
        // title view
        view.addSubview(newNoteTitleTextField)
        newNoteTitleTextField.snp.makeConstraints { (make) -> Void in
            make.top
                .equalTo(backArrowIcon.snp.bottom)
                .offset(UIConstants.PADDING_SMALL)
            
            make.leading
                .equalTo(view.safeAreaLayoutGuide.snp.leading)
                .offset(UIConstants.PADDING_SMALL)
            
            make.trailing
                .equalTo(view.safeAreaLayoutGuide.snp.trailing)
                .offset(-UIConstants.PADDING_SMALL)
        }
    }
    
    private func addSummaryView() {
        // summary view
        view.addSubview(newNoteSummaryTextField)
        newNoteSummaryTextField.snp.makeConstraints { (make) -> Void in
            make.top
                .equalTo(newNoteTitleTextField.snp.bottom)
                .offset(UIConstants.PADDING_TINY)
            
            make.height.equalTo(NewNoteUIViewController.NOTE_SUMMARY_HIGHT)
            
            make.leading
                .equalTo(view.safeAreaLayoutGuide.snp.leading)
                .offset(UIConstants.PADDING_SMALL)
            
            make.trailing
                .equalTo(view.safeAreaLayoutGuide.snp.trailing)
                .offset(-UIConstants.PADDING_SMALL)
        }
    }
    
    private func addSaveButton() {
        view.addSubview(saveButton)
        saveButton.snp.makeConstraints { (make) in
            make.top
                .equalTo(newNoteSummaryTextField.snp.bottom)
                .offset(UIConstants.PADDING_SMALL)
            
            make.trailing
                .equalTo(view.safeAreaLayoutGuide.snp.trailing)
                .offset(-UIConstants.PADDING_SMALL)
        }
    }
    
    private func addDiscardButton() {
        view.addSubview(discardButton)
        discardButton.snp.makeConstraints { (make) in
            make.top
                .equalTo(newNoteSummaryTextField.snp.bottom)
                .offset(UIConstants.PADDING_SMALL)
            
            make.trailing
                .equalTo(saveButton.snp.leading)
                .offset(-UIConstants.PADDING_SMALL)
        }
    }
    
    private func addBackArrow() {
        view.addSubview(backArrowIcon)
        backArrowIcon.snp.makeConstraints { (make) in
            make.leading.equalTo(view.safeAreaLayoutGuide.snp.leading)
                .offset(UIConstants.PADDING_SMALL)
            make.top.equalTo(view.safeAreaLayoutGuide.snp.top)
        }
    }
    
    private func bindButtons() {
        saveButton.rx.tap.bind {
            self.intent.onSaveClick(
                noteText: self.newNoteTitleTextField.text,
                noteSummary: self.newNoteSummaryTextField.text
            )
        }.disposed(by: disposeBag)
        
        discardButton.rx.tap.bind {
            self.intent.onDiscardClick()
        }.disposed(by: disposeBag)
    }
    
    @objc func handleBackClick() {
        self.intent.onBackCick(dismissFunc: didFinish)
    }
}
