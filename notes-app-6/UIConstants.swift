//
//  UiConstants.swift
//  notes-app-6
//
//  Created by Maksim Dmitriev on 20/2/22.
//

import UIKit

enum UIConstants {
    
    static let PADDING_TINY = UIScreen.main.scale * 2
    static let PADDING_SMALL = UIScreen.main.scale * 4
    static let PADDING = UIScreen.main.scale * 8
}
