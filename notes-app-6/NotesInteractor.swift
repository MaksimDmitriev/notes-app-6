//
//  NotesInteractor.swift
//  notes-app-6
//
//  Created by Maksim Dmitriev on 4/6/22.
//

protocol NotesInteractor {
    
    func add(noteText: String?, noteSummary: String)
    
    func remove(note: Note)
    
    func observeChanges()
    
    func invalidateNotificationToken()
    
    func objectAtIndexPath(index: Int) -> Note
    
    func getCount() -> Int
}
