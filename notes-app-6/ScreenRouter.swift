//
//  ScreenRouter.swift
//  notes-app-6
//
//  Created by Maksim Dmitriev on 3/5/22.
//

import UIKit

protocol ScreenRouter {
    
    func present(uiViewController: UIViewController, viewControllerToPresent: UIViewController, animated flag: Bool)
    
    func dismiss(dismissFunc: (() -> Void))
}

final class ScreenRouterImpl : ScreenRouter {
    
    
    func present(uiViewController: UIViewController, viewControllerToPresent: UIViewController, animated flag: Bool) {
        uiViewController.present(viewControllerToPresent, animated: true,completion: nil)
    }
    
    func dismiss(dismissFunc: (() -> Void)) {
        dismissFunc()
    }
}
