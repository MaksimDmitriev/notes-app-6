//
//  ViewControllerStateHandler.swift
//  notes-app-6
//
//  Created by Maksim Dmitriev on 28/5/22.
//

protocol NoteStateHandler {
    
    func handleState(state: NoteState)
}
