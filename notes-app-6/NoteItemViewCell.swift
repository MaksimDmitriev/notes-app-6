//
//  NoteItemViewCell.swift
//  notes-app-6
//
//  Created by Maksim Dmitriev on 26/4/21.
//

import UIKit

class NoteItemViewCell: UITableViewCell {
    
    static let ID = "NoteItemViewCell"
    
    let noteUILabel = UILabel()
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.addSubview(noteUILabel)
        
        noteUILabel.adjustsFontSizeToFitWidth = false
        noteUILabel.numberOfLines = 0
        noteUILabel.snp.makeConstraints { (make) -> Void in
            make.edges
                .equalToSuperview()
                .offset(12)
                .inset(12)
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
