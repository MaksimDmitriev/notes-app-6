//
//  NewNoteIntent.swift
//  notes-app-6
//
//  Created by Maksim Dmitriev on 4/6/22.
//

import UIKit

class NewNoteIntent : BaseIntent {
    
    public func onSaveClick(noteText: String?, noteSummary: String) {
        notesInteractor.add(noteText: noteText, noteSummary: noteSummary)
    }
    
    public func onDismissViewController(dimiss: () -> Void) {
        dimiss()
    }
    
    public func onDiscardClick() {
        self.stateRelay.accept(NewNoteDiscarded())
    }
    
    public func onBackCick(dismissFunc: () -> Void) {
        screenRouter.dismiss(dismissFunc: dismissFunc)
    }
}
