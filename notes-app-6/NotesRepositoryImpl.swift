//
//  NotesRepository.swift
//  notes-app-5
//
//  Created by Maksim Dmitriev on 17/4/21.
//  Copyright © 2021 Maksim Dmitriev. All rights reserved.
//

import RealmSwift
import RxSwift
import RxCocoa

class NotesRepositoryImpl : NotesRepository {
    
    // MVI state publishing
    private let stateRelay : PublishRelay<NoteState>
    
    // Open the local-only default realm
    private let localRealm = try! Realm()
    private let results : Results<Note>
    private var notificationToken : NotificationToken?
    
    init(stateRelay : PublishRelay<NoteState>) {
        self.results = localRealm.objects(Note.self)
        self.stateRelay = stateRelay
    }
    
    func add(noteText: String, summary: String) {
        try! localRealm.write {
            localRealm.add(Note(text: noteText, summary: summary))
        }
    }
    
    func remove(note: Note) {
        try! localRealm.write {
            localRealm.delete(note)
        }
    }
    
    func objectAtIndexPath(index: Int) -> Note {
        return results[index]
    }
    
    func getCount() -> Int {
        return results.count
    }
    
    func observeChanges() {
        // Get all tasks in the realm
        let tasks = results
        // Retain notificationToken as long as you want to observe
        notificationToken = tasks.observe { (changes) in
            switch changes {
            case .initial: break
                // Results are now populated and can be accessed without blocking the UI
            case .update(_, let deletions, let insertions, let modifications):
                // Query results have changed.
                if (!deletions.isEmpty) {
                    self.stateRelay.accept(NoteRemoved(deletions : deletions))
                }
                if (!insertions.isEmpty) {
                    self.stateRelay.accept(NoteAdded(insertions : insertions))
                }
            case .error(let error):
                // An error occurred while opening the Realm file on the background worker thread
                fatalError("\(error)")
            }
        }
    }
    
    func invalidateNotificationToken() {
        // Invalidate notification tokens when done observing
        notificationToken?.invalidate()
    }
}

class Note : Object {
    
    static let NOTE_TITLE_MAX_LENGTH = 300
    static let NOTE_SUMMARY_MAX_LENGTH = 600
    
    static let EMPTY = Note(text: "", summary: "")
    
    @objc dynamic var text : String = ""
    @objc dynamic var summary : String = ""
    
    convenience init(text : String, summary: String) {
        self.init()
        self.text = text
        self.summary = summary
    }
}

