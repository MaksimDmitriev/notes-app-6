//
//  BaseIntent.swift
//  notes-app-6
//
//  Created by Maksim Dmitriev on 3/6/22.
//

import RxSwift
import RxCocoa

class BaseIntent {
    
    let notesInteractor : NotesInteractor
    let screenRouter: ScreenRouter
    
    let stateRelay: PublishRelay<NoteState>
    private let disposeBag: DisposeBag
    
    private var stateHandler : NoteStateHandler?
    
    init(notesInteractor: NotesInteractor,
         screenRouter: ScreenRouter,
         stateRelay: PublishRelay<NoteState>,
         disposeBag: DisposeBag) {
        self.notesInteractor = notesInteractor
        self.screenRouter = screenRouter
        self.stateRelay = stateRelay
        self.disposeBag = disposeBag
    }
    
    public final func bind(toView view: NoteStateHandler) {
        self.stateHandler = view
        stateRelay.subscribe { event in
            guard let state = event.element else { return }
            self.stateHandler?.handleState(state: state)
        }.disposed(by: disposeBag)
    }
    
    public final func observeChanges() {
        notesInteractor.observeChanges()
    }
    
    public final func invalidateNotificationToken() {
        notesInteractor.invalidateNotificationToken()
    }
}
