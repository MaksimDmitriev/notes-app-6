//
//  Note.swift
//  notes-app-5
//
//  Created by Maksim Dmitriev on 17/4/21.
//  Copyright © 2021 Maksim Dmitriev. All rights reserved.
//

protocol NoteState {
    
}

class NoteStateDefault : NoteState {}

class NoteStateDefaultWrapper {
    
    static let DEFAULT = NoteStateDefault()
}

class NoteAdded : NoteState {
    
    let insertions: [Int]
    
    init(insertions: [Int]) {
        self.insertions = insertions
    }
}

class NoteRemoved : NoteState {
    
    let deletions: [Int]
    
    init(deletions: [Int]) {
        self.deletions = deletions
    }
}

class NewNoteDiscarded : NoteState {
}
